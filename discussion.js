db.inventory.insertMany([
   {
       "name": "JavaScript for Beginners",
       "author": "James Doe",
       "price": 5000,
       "stocks": 50,
       "publisher": "JS Publishing House"
   },
   {
       "name": "HTML and CSS",
       "author": "John Thomas",
       "price": 2500,
       "stocks": 38,
       "publisher": "NY Publishers"
   },
   {
       "name": "Web Development Fundamentals",
       "author": "Noah Jimenez",
       "price": 3000,
       "stocks": 10,
       "publisher": "Big Idea Publishing"
   },
   {
       "name": "Java Programming",
       "author": "David Michael",
       "price": 10000,
       "stocks": 100,
       "publisher": "JS Publishing House"
   }
]);

// Comparison Query Operator
// $gt/ $gte operator
/*
    Syntax:
        db.collectionName.find({"field": { $gt: value}})
        db.collectionName.find({"field": { $gte: value}})

*/
db.inventory.find({
    "stocks": {
        $gt: 50
    }
});

db.inventory.find({
    "stocks": {
        $gte: 50
    }
});

// $lt/ $lte operator
/*
   
    Syntax:
    db.collectionName.find({"field": { $lt: value}})
    db.collectionName.find({"field": { $lte: value}})
*/

db.inventory.find({
    "stocks": {
        $lt: 50
    }
});

db.inventory.find({
    "stocks": {
        $lte: 50
    }
});

// $ne operator
// Allows us to find documents that have field values that is not equal to a specified value.

/*
    Syntax:
        db.collectionName.find({field: {$ne: value}})

*/

db.inventory.find({
    "stocks": {
        $ne: 50
    }
})

//$eq operator
/*
    Syntax:
        db.collectionName.find({field: {$eq: value}})

*/

db.inventory.find({
    "stocks": {
        $eq: 50
    }
})


//$in operator
 /*
        Allos us to find documents with specific match criteria one field using different values

        Syntax:
        db.collectionName.find({field: {$in: [value1, value2] }})
*/

db.inventory.find({
    "price": {
        $in: [10000, 5000]
    }
})

//$nin operator
 /*
        Allos us to find documents with specific match criteria one field using different values

        Syntax:
        db.collectionName.find({field: {$nin: [value1, value2] }})
*/

db.inventory.find({
    "price": {
        $nin: [10000, 5000]
    }
})

// Logical Query Operators
// $or operator
/*
    Syntax:
        db.collectionName.find({
            $or: [
                {fieldA: valueA},
                {fieldB: valueB}
                ]
        })
*/

db.inventory.find({
    $or: [
        {"publisher": "JS Publishing House"},
        {"name": "HTML and CSS"}
    ]
});

db.inventory.find({
    $or: [
        {"author": "James Doe"},
        {
            "price": {
                $lte: 3000
            }
        }

    ]
});

/*
    $and operator

        Syntax:
            db.collectionName.find({ $and: [{fieldA: valueA, fieldB: valueB]})

*/

db.inventory.find({

    $and: [
        {
            "stocks": {
                $ne: 50
            }
        },
        {
            "price": {
                $ne: 5000
            }
        }
    ]
});

//by default
db.inventory.find({
            "stocks": {
                $ne: 50
        },
            "price": {
                $ne: 5000
        }
});


// Field Projection
// Inclusion
/*
    Syntax:
        db.collectionName.find({criteria},{field:1})
*/

db.inventory.find({
    "publisher": "JS Publishing House"
    },
    {
        "name": 1,
        "author": 1,
        "price": 1
    }

);

// Exclusion
/*
    Syntax:
        db.colletionName.find({criteria}, field: 0)

*/

db.inventory.find(
    {
        "author": "John Thomas"
    },
    {
        "price": 0,
        "stocks": 0,
    }
)

// Suppressing the ID field

db.inventory.find( 
    {
        "price": {
            $lte: 5000
        }
    },
    {
        "_id": 0,
        "name": 1,
        "author": 1
    }
);

// bawal pagsabay ung field projection at field exclusion, unless pag id ung ineexclude

// Evaluation Query Operators
// $regex operator

/*
    Syntax:
        db.collectionName.find({field: $regex: 'pattern', $options: '$optionValue'})
*/

// Case sensitive query
db.inventory.find({
    "author": {
        $regex: 'M'
    }
})

// Case Insensitive
db.inventory.find({
    "author": {
        $regex: 'M',
        $options: '$i'
    }
});
